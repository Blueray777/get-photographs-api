// server.js
'use strict'

const express = require('express');
//const morgan = require('morgan');
const path = require('path');
//const mongoose = require('mongoose');
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');
const config = require('./config/config');
const expressPHP = require('./expressPHP')();
//const routes = require('./app/src/js/routes');
const PORT = config.APP_PORT || 5000;
const app = express();
// expressPHP
expressPHP.phpFolder = './app/dist';

// connect to database
//mongoose.connect(config.DB);

// Use morgan to log request in dev mode
//app.use(morgan('dev'));

// Set environments
app.use(cookieParser());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
  extended: true
}));

// Set static files from /app/htdocs path
app.use(express.static(path.join(__dirname, '/app/dist')));

// use routes defined in routes.js
//app.use('/', routes);

// set php
app.use('*.php', (req, res, next) => {
  expressPHP.parseFile(req.originalUrl, (phpResult) => {
    res.write(phpResult);
    res.end();
  })
})

// // XHR
// app.use((req, res, next) => {
//   // Website allow to content
//   res.setHeader('Access-Control-Allow-Origin', 'http://localhost:' + PORT)
//   // Request methods
//   res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE')
//   // Request headers
//   res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type')
//   // Pass to next layer of middleware
//   next()
// })

app.get('/', function (req, res) {
  res.sendfile('index.html');
});
// Routes
app.listen(PORT, '0.0.0.0', function () {
  console.log('Node server listening port '+ PORT +' ;)');
});
