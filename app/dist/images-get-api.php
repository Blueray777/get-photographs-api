<?php
$json_url = "http://localhost:5000/json/photo-list.json";
$filter = $_GET['filter'];


// cURLセッションを初期化
$ch_json = curl_init();

// オプションを設定
curl_setopt($ch_json, CURLOPT_URL, $json_url); // 取得するURLを指定
curl_setopt($ch_json, CURLOPT_RETURNTRANSFER, true); // 実行結果を文字列で返す
curl_setopt($ch_json, CURLOPT_SSL_VERIFYPEER, false); // サーバー証明書の検証を行わない

// URLの情報を取得
$responseJson = curl_exec($ch_json);
// セッションを終了
curl_close($conn);

// 取得結果を連想配列に変換
$resultJson = json_decode($responseJson, true);


// All Data
if($filter === 'All'){
  foreach ($resultJson as $key => $value) {
    $img_url = $value['img_url'];
    $imgArray[] = array(
      'img' => $img_url
    );
  }
}

// filter Data
if($filter == 'Fuji' || $filter == 'GR' || $filter == 'Olympus'){
  foreach ($resultJson as $key => $value) {
    $type = $value['Camera'];
    if($type === $filter){
      $imgArray[]= array(
      'img' => $value['img_url']
    );
    }
  }
} else if ($filter == 'Street' || $filter == 'Life' || $filter == 'Dogs'){
  foreach ($resultJson as $key => $value) {
    $type = $value['Genre'];
    if($type === $filter){
      $imgArray[]= array(
      'img' => $value['img_url']
    );
    }
  }
}

// ----------------------------------------- ページ割り

// 最初に表示させる件数
define('FIRSTLOOK', 16);
// 1ページの写真の表示数
define('MAX', 8);

$disp_data = array();
// トータル写真データ数
$imgArray_num = count($imgArray);

// トータルページ数 ※ceilは小数点を切り捨てる関数
$max_page = ceil(($imgArray_num - (FIRSTLOOK - MAX)) / MAX);
if(!isset($_GET["page"])){
	$page = 1;
} else {
	$page = $_GET["page"];
}

// 配列の何番目から取得したら良いかを計算
$start_num = $page * MAX;

// 写真の数が初見表示数より多かったページングする
if($imgArray_num >= FIRSTLOOK){
  if($page == 1){
    $disp_data = array_slice($imgArray, 0, FIRSTLOOK);
  } else if($page == 2) {
    $disp_data = array_slice($imgArray, FIRSTLOOK, MAX);
  } else if($page >= 3 && $page < $max_page){
    $disp_data = array_slice($imgArray, $start_num, MAX);
  } else if ($page == $max_page){
    $disp_data = array_slice($imgArray, $start_num);
  } else if ($page > $max_page){
    $disp_data = null;
  }
} else if ($imgArray_num < FIRSTLOOK){
  if($page == 1){
    $disp_data = $imgArray;
  }
  if($page == 2){
    $disp_data = null;
  }
}

echo json_encode($disp_data, JSON_UNESCAPED_UNICODE);
?>
