'use strict';

// images-grid.js

// Murri Setting
import 'web-animations-js/web-animations-next.min.js';
const Muuri = require('muuri');

// setting
const $gridArea = document.querySelector('.imgGrid-post_list');
const $imgGrid = document.getElementById('imgGrid-post');
const $postMesse = document.getElementById('imgGrid-post__messe');
const $moreBtn = document.querySelector('.imgGrid-post-moreBtn');
const $closeBtn = document.querySelector('.imgGrid-post-closeBtn');
const $filterBtn = document.getElementsByClassName('imgGrid-nav--btn');
let filterBtnLen = $filterBtn.length;
let page = '';
const all = 'All';
let grid;

// event
window.addEventListener('load', () => {
  page = 1;
  loadPost(page, all);
  // ------------------------------------ Genre & Camera Filter
  // Navi
  const $torigger = document.querySelector('.imgGrid-nav--triggerIcon');
  const $filterNav = document.querySelector('.imgGrid-nav');
  $torigger.addEventListener('click', function (event) {
    $filterNav.classList.toggle('is-open');
  });

  // Cookie Set & each photo desplay
  for (let i = 0; i < filterBtnLen; i++) {
    let filterval = '';
    let titleJanle = document.getElementById('js-titleJanle');
    let janleName = '';
    $filterBtn[i].addEventListener('click', () => {
      // data属性を調べる
      filterval = $filterBtn[i].dataset.filter;
      // ジャンル名を調べる
      janleName = $filterBtn[i].textContent;
      // page を初期化
      page = 1;
      // 選択されていないジャンルのボタン押下で
      // 表示してある写真を消してデフォルトである全ての写真表示に切り替える
      // 選択されているジャンルのボタン押下でXHRを呼び出さない
      let hasClass = $filterBtn[i].classList.contains('is-active');
      if (!hasClass) {
        removeGrid(0);
        loadPost(page, filterval);
        titleJanle.innerHTML = janleName;
      }
      // スマホ表示の時にジャンルボタンを押下でナビを閉じる
      let hasOpenClass = $filterNav.classList.contains('is-open');
      if (hasOpenClass) {
        $filterNav.classList.remove('is-open');
      }
      // 自身以外に'.is-active'を持っていたら外す
      let hasIsActive = document.getElementsByClassName('is-active');
      let hasIsActiveLen = hasIsActive.length;
      if (hasIsActiveLen > 0) {
        for (let j = 0; j < hasIsActiveLen; j++) {
          hasIsActive[j].classList.toggle('is-active');
        }
      }
      // 自身に'.is-active'をつける
      $filterBtn[i].classList.toggle('is-active');
    }, false);
  }
});
// ----------------------------------------- ボタンイベント
// Closeボタン event
$closeBtn.addEventListener('click', function (event) {
  // 初期表示数16枚を残して写真を削除
  removeGrid();
  $closeBtn.classList.add('display-none');
  page = 1;
}, false);

// Moreボタンevent
$moreBtn.addEventListener('click', function (event) {
  page++;
  let hasClass = document.getElementsByClassName('is-active');
  let filterval = '';
  for (let i = 0; i < hasClass.length; i++) {
    filterval = hasClass[i].dataset.filter;
  };
  loadPost(page, filterval);
}, false);

// ---------------------------------------------- grid layout

async function loadImages (imgSrc) {
  const promises = [];
  const images = [];
  imgSrc.forEach(function (url) {
    var promise = new Promise(function (resolve) {
      const img = new Image();
      img.onload = function () {
        resolve();
      };
      img.src = url;

      images.push(img);
    });
    promises.push(promise);
  });
  /// すべて読込完了まで待つ
  await Promise.all(promises);
}

async function gridDisplay (imgSrc) {
  await loadImages(imgSrc);
  if ($gridArea) {
    grid = new Muuri($gridArea, {
      items: $gridArea.querySelectorAll('.imgGrid-post_item'),
      layoutOnResize: 200,
      hideDuration: 1000
    });
  }
}

// add Items
async function gridAddItems (elems, imgSrc) {
  await loadImages(imgSrc);
  grid.add(elems);
}

// Grid item remove
function removeGrid () {
  grid.destroy(true);
  if ($gridArea) {
    grid = new Muuri($gridArea, {
      items: $gridArea.querySelectorAll('.imgGrid-post_item'),
      layoutOnResize: 200,
      hideDuration: 1000
    });
  }
}

// ------------------------------------ Display Photo

// create post ariticle Elements
function createElm (img, href) {
  const imgSrc = img;
  const aHref = href;
  // Create and Set DOM
  const li = document.createElement('li');
  const aItem = document.createElement('a');
  const imgItem = document.createElement('img');
  li.classList.add('imgGrid-post_item');
  aItem.classList.add('modal');
  aItem.setAttribute('href', aHref);
  aItem.setAttribute('target', '_blank');
  imgItem.classList.add('imgGrid-post-image');
  imgItem.setAttribute('src', imgSrc);
  aItem.appendChild(imgItem);
  li.appendChild(aItem);
  return li;
};

// POST Articles DATA Ajax
function loadPost (page, filter) {
  // XmlHttpRquest
  const xhr = new XMLHttpRequest();
  const method = 'GET';
  const url = 'images-get-api.php?page=' + page + '&filter=' + filter;
  xhr.open(method, url, true);
  xhr.responseType = 'json';
  xhr.onreadystatechange = () => {
    if (xhr.readyState === 4 && xhr.status === 200) {
      let data = xhr.response;
      let dataLen;
      if (data !== null) {
        dataLen = data.length;
      } else {
        $moreBtn.classList.add('display-none');
        $closeBtn.classList.remove('display-none');
      }
      // 投稿データが取得できたら（投稿が1件以上あったら）
      // 投稿を表示させる。
      if (dataLen === 0) {
        $postMesse.classList.remove('display-none');
      } else if (dataLen > 0) {
        $imgGrid.classList.remove('display-none');
        const fragment = document.createDocumentFragment();
        let href = '';
        let img = '';
        let imgArray = [];
        let elems = [];

        // APIから表示に必要なデータを抽出し配列にする
        for (let i = 0; i < dataLen; i++) {
          img = data[i].img;
          href = data[i].img;
          imgArray.push(data[i].img);
          let elem = createElm(img, href);
          fragment.appendChild(elem);
          elems.push(elem);
        }
        // append elements to container
        $gridArea.appendChild(fragment);
        if (page === 1 && filter === 'All') {
          // 画像データの読み込み完了後にグリッドレイアウトを整列させる
          gridDisplay(imgArray);
        } else if (page >= 1 && filter === 'All') {
          // gridDisplay(imgArray);
          gridAddItems(elems, imgArray);
        } else if (page >= 1 && filter !== 'All') {
          // gridDisplay(imgArray);
          gridAddItems(elems, imgArray);
        }

        modalOpenEvent(elems);
        const closeHasDisplayNone = $closeBtn.classList.contains('display-none');
        const moreHasDisplayNone = $moreBtn.classList.contains('display-none');
        const $gridItem = document.getElementsByClassName('imgGrid-post_item');
        let gidItemLen = $gridItem.length;
        if (dataLen === 16 || dataLen === 8) {
          if (moreHasDisplayNone) {
            $moreBtn.classList.remove('display-none');
          }
          if (!closeHasDisplayNone) {
            $closeBtn.classList.add('display-none');
          }
        } else if (dataLen < 16 || dataLen >= 8) {
          if (!moreHasDisplayNone) {
            $moreBtn.classList.add('display-none');
          }
          if (!closeHasDisplayNone) {
            $closeBtn.classList.add('display-none');
          }
          if (gidItemLen > 16) {
            if (closeHasDisplayNone) {
              $closeBtn.classList.remove('display-none');
            }
          }
        } else if (dataLen < 8) {
          if (!moreHasDisplayNone) {
            $moreBtn.classList.add('display-none');
          }
          if (!closeHasDisplayNone) {
            $closeBtn.classList.add('display-none');
          }
          if (gidItemLen > 16) {
            if (closeHasDisplayNone) {
              $closeBtn.classList.remove('display-none');
            }
          }
        }
      } else {
        return false;
      }
    }
  };
  xhr.send(null);
}

// ---------------------------------------------------- Modal
// modalを呼び出すaタグには'.modal' をつける
// モーダルに内包するHTMLを呼び出す場合には、呼び出し先要素に'.modal-inner'をつける

// modal open
function modalOpen (target) {
  // setting
  const $body = document.querySelector('body');
  const $overLay = document.createElement('div');
  const $overLayIn = document.createElement('div');
  const $modalWall = document.createElement('div');
  const $modalContents = document.createElement('div');
  const $closeElm = document.createElement('div');
  const $modalInner = document.createElement('div');
  const $imageElm = document.createElement('img');
  // モーダルに必要なDOMを作成・追加
  $overLay.setAttribute('id', 'overLay');
  $overLayIn.setAttribute('id', 'overLay-inner');
  $modalWall.classList.add('modal-wrapper');
  $closeElm.classList.add('modal-close');
  $closeElm.innerHTML = '<i class="fas fa-times"></i>';
  $modalContents.classList.add('modal-contetns');
  $overLay.appendChild($overLayIn);
  $modalWall.appendChild($modalContents);
  $modalContents.appendChild($closeElm);
  $body.appendChild($modalWall);
  $body.appendChild($overLay);
  // targetが画像ファイルか判定しimg要素を作成
  if (target.match(/.(jpg|jpeg|png|gif)$/i)) {
    $modalInner.classList.add('modal-inner');
    $imageElm.setAttribute('src', target);
    $imageElm.classList.add('madal-img');
    $modalInner.appendChild($imageElm);
    $modalContents.appendChild($modalInner);
  } else {
    $modalContents.appendChild(target);
  }
  // Get Modal size
  modalSize();
  // Modal Overlay Close
  modalCloseOverlayEvent();
  // Modal Close
  modalCloseEvent();
}

// modal Close
function modalClose () {
  const $body = document.querySelector('body');
  const $overLay = document.getElementById('overLay');
  const $modalWall = document.querySelector('.modal-wrapper');
  // モーダル背景と要素を削除
  $body.removeChild($modalWall);
  $body.removeChild($overLay);
  $body.classList.remove('modal-open');
}

// modal open event
function modalOpenEvent (elems) {
  // XHR で読み込んだデータだけにイベントを追加
  const modalElms = elems;
  const modalElmLen = modalElms.length;
  for (let i = 0; i < modalElmLen; i++) {
    modalElms[i].addEventListener('click', (event) => {
      let modalSelecter = modalElms[i].querySelector('.modal');
      // aタグのhref属性に設定されている中身を取得
      let target = modalSelecter.getAttribute('href');
      // aタグのデフォルト機能をブロック
      event.preventDefault();
      // モーダル表示の関数呼び出し
      modalOpen(target);
    }, false);
  }
}

// modal close event - osverLay
function modalCloseOverlayEvent () {
  const modalOverLay = document.getElementById('overLay-inner');
  modalOverLay.addEventListener('click', modalClose, false);
}

// modal close event
function modalCloseEvent () {
  const modalCloseElm = document.querySelectorAll('.modal-close');
  const modalCloseElmLen = modalCloseElm.length;
  for (let i = 0; i < modalCloseElmLen; i++) {
    modalCloseElm[i].addEventListener('click', modalClose, false);
  }
}

// モーダルのサイズを取得して画面に配置する
function modalSize () {
  const $body = document.querySelector('body');
  const $modalWall = document.querySelector('.modal-wrapper');
  const modalWallHeight = $modalWall.clientHeight;
  // モーダル高さが画面高さより長いとき
  if (modalWallHeight >= window.innerHeight) {
    const hasModalOpenClass = $body.classList.contains('modal-open');
    $modalWall.style.top = '34px';
    $modalWall.style.transform = 'translateY(34px)';
    window.scrollTo(0, 0);
    if (hasModalOpenClass) {
      $body.classList.remove('modal-open');
    }
  } else {
    $body.classList.add('modal-open');
    $modalWall.style.top = '';
    $modalWall.style.marginLeft = '';
    $modalWall.style.transform = '';
  }
}
// モーダルリサイズ対応
window.addEventListener('resize', (event) => {
  modalSize();
}, false);
