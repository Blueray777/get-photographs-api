'use strict';
const path = require('path');
const webpack = require('webpack');
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
// ソースマップの利用有無(productionのときはソースマップを利用しない)
const devMode = process.env.NODE_ENV !== 'production'

module.exports = {
  // モード値を production に設定すると最適化された状態で、
  // development に設定するとソースマップ有効でJSファイルが出力される
  mode: "development",
  devtool: 'source-map',
  //メインのJS（エントリーポイント）
  entry: {
    'script': [
      path.resolve(__dirname, './app/src/js/index')
    ]
  },
  // ファイルの出力設定
  output: {
    // 出力ファイルのディレクトリ設定
    path: path.resolve(__dirname, './app/dist/'),
    // 出力ファイルの名前
    filename: "js/[name].js"
  },
  // webpack4はlordersではなくなりました
  module: {
    rules: [{
        test: /\.(sa|sc|c)ss$/,
        use: [
          // devMode ? 'vue-style-loader' : MiniCssExtractPlugin.loader,
          // 0 => no loaders (default);
          // 1 => postcss-loader;
          // 2 => postcss-loader, sass-loader
          {
            loader: MiniCssExtractPlugin.loader
          },
          {
            loader: 'css-loader',
            options: {
              url: false,
              sourceMap: true,
              importLoaders: 2
            }
          },
          {
            loader: 'postcss-loader',
            options: {
              sourceMap: true,
              plugins: [
                require('autoprefixer')({
                  // ☆IEは11以上、Androidは4.4以上
                  // その他は最新2バージョンで必要なベンダープレフィックスを付与する設定
                  browsers: ["last 2 versions", "ie >= 11", "Android >= 4"]
                })
              ]
            }
          },
          {
            loader: 'sass-loader',
            options: {
              sourceMap: true
            }
          },
        ],
      },
      {
        test: /\.pug$/,
        loader: 'pug-plain-loader',
        oneOf: [
          // this applies to pug imports inside JavaScript
          // Javascript内に pug を インポートするための設定
          {
            use: ['raw-loader', 'pug-plain-loader']
          }
        ]
      },
      {
        enforce: 'pre',
        test: /\.(js)$/,
        loader: 'eslint-loader',
        exclude: /node_modules/
      },
      // 拡張子.jsのファイルに対する設定
      {
        test: /\.js$/,
        exclude: file => (
          /node_modules/.test(file)
        ),
        use: [{
          loader: 'babel-loader',
        }]
      },
    ]
  },
  // プラグインを列挙
  plugins: [
    new MiniCssExtractPlugin({
      // Options similar to the same options in webpackOptions.output
      // both options are optional
      filename: 'css/style.css',
    }),
    new webpack.ProvidePlugin({
      $: 'jquery',
      jQuery: 'jquery'
    })
  ],
}
